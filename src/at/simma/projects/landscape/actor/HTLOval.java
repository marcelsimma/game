package at.simma.projects.landscape.actor;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class HTLOval implements Actor {
	private float width, height, x, y, speed;
	private List<Shape> collissionPartners;
	private Shape shape;

	public HTLOval(float x, float y, float  width, float height, float speed) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape = new Rectangle(x, y, 50	, 50);
		this.collissionPartners = new ArrayList<>();
	}

	public void update(GameContainer container, int delta) {
		this.y += speed;
		if (y >= 620) {
			this.y = -20;
		}
		
		this.shape.setY(this.y);

		for (Shape s : collissionPartners) {
			if (s.intersects(this.shape)) {
				System.out.println("Collission!");
			}
		}
	}
	
	public void addCollissionPartner(Shape shape) {
		this.collissionPartners.add(shape);
	}
	
	public Shape getShape() {
		return this.shape;
	}

	public void render(Graphics graphics) {
		//graphics.drawOval((float) this.x, (float) this.y, (float) this.width, (float) this.height);
		graphics.draw(this.shape);
	}
}
