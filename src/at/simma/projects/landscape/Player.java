package at.simma.projects.landscape;

import java.util.ArrayList;
import org.newdawn.slick.geom.Rectangle;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.simma.projects.landscape.actor.Actor;
import at.simma.projects.landscape.actor.Canon;

public class Player implements Actor {
	private double width, height, x, y, speed, centerX, centerY;
	Landscape landscape;
	private int cooldown;
	private Image image;
	private List<Shape> collissionPartners;
	private Shape shape;
	private ArrayList<Rectangle> rectangles;

	
	public Player(double x, double y, double width, double height, double speed, Landscape landscape) throws SlickException {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.landscape = landscape;
		this.cooldown = 0;
		this.shape = new Rectangle((float)x, (float)y, (float)this.height, (float)this.width);
		this.collissionPartners = new ArrayList<>();
		this.image = new Image("images/snowflake.png");

	}
	
	public void addCollissionPartner(Shape shape) {
		this.collissionPartners.add(shape);
	}
	
	public Shape getShape() {
		return this.shape;
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y--;
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y++;
		}
		if (this.cooldown > 500) {
			if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
				landscape.addActor(new Canon(this.x + this.width, this.y, 10, 10, 1, 10, 10));
				System.out.println("space");
			}
			cooldown = 0;
		} else {
			cooldown += delta;
		}
		this.shape.setY((float)this.y);
		this.shape.setX((float)this.x);
	}
	

	

	@Override
	public void render(Graphics graphics) {
		//graphics.drawRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);
		image.draw((float)this.x, (float)this.y, (float)this.width, (float)this.height);
		graphics.draw(shape);
		graphics.fillOval((float)this.x, (float)this.y, (float)this.width, (float)this.height);
	}

}
