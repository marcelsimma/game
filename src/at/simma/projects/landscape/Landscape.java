package at.simma.projects.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import at.simma.projects.landscape.actor.Actor;
import at.simma.projects.landscape.actor.HTLCircle;
import at.simma.projects.landscape.actor.HTLOval;
import at.simma.projects.landscape.actor.HTLRect;
import at.simma.projects.landscape.actor.Picture;
import at.simma.projects.landscape.actor.Snowflake;

public class Landscape extends BasicGame {

	private HTLRect rectangle1;
	private HTLCircle circle;
	private HTLOval oval;
	private Snowflake flake;
	private ArrayList<Actor> actors;
	private Player player;
	private ArrayList<Actor> actorsToAdd;
	private Snowflake snowflake;
	
	public Landscape() {
		super("Landscape");
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		this.actors = new ArrayList<>();
		this.actorsToAdd = new ArrayList<>();
		
		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflake("small"));
			this.actors.add(new Snowflake("middle"));
			this.actors.add(new Snowflake("large"));
		}
		player = new Player(10, 10, 10, 10, 0.01, this);
		HTLOval o1 = new HTLOval(10,10,10,10,0.1f);
		o1.addCollissionPartner(player.getShape());
		
 		actors.add(player);
 		actors.add(o1);
 		
 		//SHTLOval o1 = new HTLOval(10,10,10,10,0.1);
  		//this.snowflake = new Snowflake("middle");
 	//	snowflake.addActor(o1.getShape());
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for (Actor actor : actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : actors) {
			actor.update(gc, delta);
		}
		
		if (this.actorsToAdd.size()>0) {
			for (Actor a : this.actorsToAdd) {
				this.actors.add(a);
			}
			this.actorsToAdd.clear();
		}

	}

	void addActor(Actor actor) {
		this.actorsToAdd.add(actor);
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(1920, 1080, true);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
