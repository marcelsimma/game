package at.simma.projects.zatacka;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class StartScreen {
	private float width, height, x, y;
	private Color color;

	private TextButton startButton;

	public TextButton getStartButton() {
		return this.startButton;
	}

	public static ArrayList<TextButton> buttons;
	public StartScreen(float x, float y, float width, float height, Color color) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.color = color;

		this.startButton = new TextButton(Game.BORDER_SPACE,
				Game.BORDER_SPACE + 200 + ((Game.chooseablePlayers.size() + 1) * 50), 200, 50, new Color(0x00025472),
				Color.white, "Spiel starten");

		buttons = new ArrayList<>();

		for (Player p : Game.chooseablePlayers) {
			int playerIndex = (Game.chooseablePlayers.indexOf(p) + 1) * 50;
			buttons.add(new TextButton(Game.BORDER_SPACE + 850, Game.BORDER_SPACE + 160 + playerIndex, 212, 35,
					new Color(0x00025472), Color.white, "nicht ausgew�hlt"));
		}
		

	}
	
	
	
	public void update(GameContainer gc, int delta) {
		/* never used */
	}

	public void render(Graphics graphics) {
		/* background */
		graphics.setColor(this.color);
		graphics.fillRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);

		/* heading */
		Game.ttfHeading.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE, "Spiel starten", Color.white);
		Game.ttfSubHeading.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE + 80,
				"Um das Spiel zu starten, m�ssen mindestens zwei Spieler ausgew�hlt sein.", Color.white);

		Game.ttfTableHeading.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE + 160, "Spielername", Color.white);
		Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 250, Game.BORDER_SPACE + 160, "KeyLeft", Color.white);
		Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 450, Game.BORDER_SPACE + 160, "KeyRight", Color.white);
		Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 650, Game.BORDER_SPACE + 160, "Farbe", Color.white);
		Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 850, Game.BORDER_SPACE + 160, "Ausgew�hlt", Color.white);

		for (Player p : Game.chooseablePlayers) {
			int playerIndex = (Game.chooseablePlayers.indexOf(p) + 1) * 50;
			Game.ttfTableText.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE + 160 + playerIndex, p.getPlayerName(),
					Color.white);
			Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 250, Game.BORDER_SPACE + 160 + playerIndex,
					p.getKeyLeftName(), Color.white);
			Game.ttfTableHeading.drawString(Game.BORDER_SPACE + 450, Game.BORDER_SPACE + 160 + playerIndex,
					p.getKeyRightName(), Color.white);
			Game.ttfTableText.drawString(Game.BORDER_SPACE + 650, Game.BORDER_SPACE + 160 + playerIndex,
					p.getColorName(), Color.white);
		}
		for (TextButton b : buttons) {
			b.render(graphics);
		}
		startButton.render(graphics);
	}

	public ArrayList<TextButton> getButtons() {
		return this.buttons;
	}

}
