package at.simma.projects.zatacka;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.Graphics;

public class Player {
	float x, y, angle;
	private int keyLeft, keyRight, counter = 0;
	private Color color;
	private ArrayList<MyCircle> circles;
	private ArrayList<Shape> shapes;
	private Shape shape;
	private boolean alive;
	private String colorName, keyRightName, keyLeftName, playerName;

	public Player(int keyLeft, String keyLeftName, int keyRight, String keyRightName, Color color, String colorName,
			String playerName) {

		this.keyLeft = keyLeft;
		this.keyLeftName = keyLeftName;
		this.keyRight = keyRight;
		this.keyRightName = keyRightName;
		this.color = color;
		this.colorName = colorName;
		this.playerName = playerName;

		this.x = Game.BORDER_SPACE + (float) (Math.random() * (Game.PLAYGROUND_WIDTH - Game.BORDER_SPACE * 2));
		this.y = Game.BORDER_SPACE + (float) (Math.random() * (Game.WINDOW_HEIGHT - Game.BORDER_SPACE * 2));
		this.angle = (float) Math.random() * 360;
		this.circles = new ArrayList<>();
		this.shapes = new ArrayList<>();
		this.alive = false;
	}

	public void update(GameContainer gc, int delta) {
		// neue Posioin des Kopfes
		newPos(gc, delta);
		// Verlängerung der Schlange
		snakeGrowth();
		// Verkürzung der Schlange
		snakeReduction();
	}

	public void render(Graphics graphics) {
		for (MyCircle c : circles) {
			c.render(graphics);
		}
		// Schlangenkopf zeichnen
		graphics.setColor(Game.PLAYER_HEAD_COLOR);
		graphics.fillOval(this.x, this.y, Game.PLAYER_RADIUS*2, Game.PLAYER_RADIUS*2);
	}

	private void newPos(GameContainer gc, int delta) {
		getKeyInput(gc, this.keyLeft, -0.1f, delta);
		getKeyInput(gc, this.keyRight, 0.1f, delta);

		this.x += (float) Math.cos(Math.toRadians(angle)) * delta / 10;
		this.y += (float) Math.sin(Math.toRadians(angle)) * delta / 10;

		// Neue Positionierung des Shapes des Schlangenkopfes
		this.shape = newCircle();
	}
	private void snakeGrowth() {
		circles.add(new MyCircle(this.x, this.y, Game.PLAYER_RADIUS*2, this.color));
		shapes.add(newCircle());
	}

	private void snakeReduction() {
		if ((this.counter++ % 2) == 0 || (this.counter % 3) == 9) {
			this.circles.remove(0);
			this.shapes.remove(0);
		}
	}

	public void setAlive() {
		if (this.alive == true)
			this.alive = false;
		else
			this.alive = true;
	}
	
	private Circle newCircle() {
		return new Circle(this.x + Game.PLAYER_RADIUS, this.y + Game.PLAYER_RADIUS, Game.PLAYER_RADIUS);
	}

	public boolean getAlive() {
		return alive;
	}

	public void getKeyInput(GameContainer gc, int key, float number, int delta) {
		if (gc.getInput().isKeyDown(key)) {
			this.angle += number * delta;
		}
	}

	public String getKeyRightName() {
		return keyRightName;
	}

	public String getKeyLeftName() {
		return keyLeftName;
	}

	public int getKeyLeft() {
		return keyLeft;
	}

	public int getKeyRight() {
		return keyRight;
	}

	public String getColorName() {
		return colorName;
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public ArrayList<MyCircle> getCircles() {
		return circles;
	}
	
	public boolean setAliveFalse() {
		if (this.alive == true) {
			this.alive = false;
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	public Shape getShape() {
		return this.shape;
	}

}