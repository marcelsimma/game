package at.simma.projects.zatacka;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

public class EndScreen {
	private float width, height, x, y;
	private Color color;
	private String message;
	private TextButton startButton;

	public EndScreen(float x, float y, float width, float height, Color color, String playerName) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.color = color;

		this.message = playerName + " hat das Spiel gewonnen!";
		this.startButton = new TextButton(Game.BORDER_SPACE, Game.BORDER_SPACE + 150, 200, 50, new Color(0x00025472),
				Color.white, "Hauptmen�");
	}

	public void render(Graphics graphics) throws SlickException {
		/* screen background */
		graphics.setColor(this.color);
		graphics.fillRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);

		/* heading */
		Game.ttfHeading.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE, "Spiel beendet", Color.white);

		/* winner */
		Game.ttfSubHeading.drawString(Game.BORDER_SPACE, Game.BORDER_SPACE + 80, message, Color.white);

		/* start button */
		startButton.render(graphics);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TextButton getStartButton() {
		return this.startButton;
	}
}
