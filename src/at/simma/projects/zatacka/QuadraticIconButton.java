package at.simma.projects.zatacka;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class QuadraticIconButton {
	private float width, height, x, y;
	private Color color;
	private Image image;

	public QuadraticIconButton(float x, float y, float width, float height, Color color, String imagePath) throws SlickException {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
		this.image = new Image(imagePath);
	}

	public void render(Graphics graphics) {
		/* button background */
		graphics.setColor(color);
		graphics.fillRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);
		/* icon */
		this.image.draw(this.x+this.width/4, this.y+this.height/4, this.width/2, this.height/2);
	}
	
	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public Color getColor() {
		return color;
	}

	public Image getImage() {
		return image;
	}
}
