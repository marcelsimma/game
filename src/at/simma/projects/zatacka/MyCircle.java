package at.simma.projects.zatacka;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class MyCircle {
	private float x, y, radius;
	private Color color;

	public MyCircle(float x, float y, float radius, Color color) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
	}

	public void render(Graphics graphics) {
		graphics.setColor(this.color);
		graphics.fillOval(this.x, this.y, this.radius, this.radius);
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
}
