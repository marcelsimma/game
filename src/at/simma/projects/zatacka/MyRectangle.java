package at.simma.projects.zatacka;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class MyRectangle{
	private float width, height, x, y;
	private Color color;
	private Shape shape;

	public MyRectangle(float x, float y, float width, float height, Color color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
		this.shape = new Rectangle(this.x+Game.PLAYER_RADIUS/2, this.y+Game.PLAYER_RADIUS*2, this.width-Game.PLAYER_RADIUS*2, this.height-Game.PLAYER_RADIUS*35/10);
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(color);
		graphics.fillRect((float)this.x, (float)this.y, (float)this.width, (float)this.height);
	}
	
	public Shape getShape() {
		return this.shape;
	}
	
	
}
