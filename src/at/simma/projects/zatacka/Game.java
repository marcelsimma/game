package at.simma.projects.zatacka;

import java.awt.Font;
import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Shape;

public class Game extends BasicGame {
	/* global application settings */
	public static final int WINDOW_WIDTH = 1920, WINDOW_HEIGHT = 1080, PLAYER_RADIUS = 7, PLAYGROUND_WIDTH = WINDOW_WIDTH,
			BORDER_SPACE = 80;
	private int applicationMode = 0;

	/* global fonts */
	public static final Font FONT_HEADING = new Font("Calibri Light", Font.PLAIN, 55),
			FONT_SUBHEADING = new Font("Calibri Light", Font.PLAIN, 32),
			FONT_TABLE_HEADING = new Font("Calibri Light", Font.BOLD, 28),
			FONT_TABLE_TEXT = new Font("Calibri Light", Font.PLAIN, 28),
			FONT_BUTTON_TEXT = new Font("Calibri Light", Font.PLAIN, 25);
	public static TrueTypeFont ttfHeading, ttfSubHeading, ttfTableHeading, ttfTableText, ttfButtonText;

	/* playground */
	private MyRectangle playground;
	private int alivePlayers = 0;
	public static ArrayList<Player> players, chooseablePlayers;
	public static final Color PLAYER_HEAD_COLOR = new Color(0x00fcc205);

	/* startscreen */
	private StartScreen startScreen;

	/* endscreen */
	private EndScreen endscreen;

	/* sidebar */
//	private MyRectangle sideBar;
	private QuadraticIconButton closeApplication;

	public Game() {
		super("Achtung, die Kurve!");
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		/* global fonts */
		initGlobalFonts();

		/* playground */
		initPlayground();

		/* startscreen */
		initStartScreen();

		/* endscreen */
		this.endscreen = new EndScreen(0, 0, PLAYGROUND_WIDTH, WINDOW_HEIGHT, new Color(0x00006f96), "Hallo");

		/* sidebar */
//		this.sideBar = new MyRectangle(PLAYGROUND_WIDTH, 0, WINDOW_WIDTH - PLAYGROUND_WIDTH, WINDOW_HEIGHT,
//				new Color(0x000085b4));
		this.closeApplication = new QuadraticIconButton(WINDOW_WIDTH - 50, 0, 50, 50, Color.transparent,
				"images/icon_closeApplication.png");
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		if (this.applicationMode == 0) {
			startScreen.render(graphics);
		}
		if (this.applicationMode == 1) {
			this.playground.render(graphics);
			for (Player p : players) {

				p.render(graphics);
			}
		}
		if (this.applicationMode == 2) {
			endscreen.render(graphics);
		}

//		this.sideBar.render(graphics);
		this.closeApplication.render(graphics);

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		getKeyInput(gc, Input.KEY_ESCAPE);
		if (this.applicationMode == 1) {
			if (this.alivePlayers > 1) {
				for (Player head : players) {
					if (head.getAlive() == true && this.alivePlayers > 1) {
						for (Player shaft : players) {
							if (head != shaft) {
								for (Shape s : shaft.getShapes()) {
									if (s.intersects(head.getShape())
											|| !head.getShape().intersects(playground.getShape())
													&& head.setAliveFalse() == true) {
										alivePlayers -= 1;
									}
								}
							}
						}
					}
				}
			}
			if (this.alivePlayers > 1) {
				for (Player p : players) {
					if (p.getAlive() == true) {
						p.update(gc, delta);
					}
				}
			} else {
				for (Player p : players) {
					if (p.getAlive() == true) {
					delay(600);
						this.endscreen.setMessage(p.getPlayerName() + " hat das Spiel gewonnen!");
						this.applicationMode = 2;
						initPlayground();
					}
				}
			}
		}
		if (this.applicationMode == 0) {
			this.startScreen.update(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Game());
			container.setDisplayMode(WINDOW_WIDTH, WINDOW_HEIGHT, true);
			container.setShowFPS(false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public void mouseClicked(int button, int x, int y, int clickCount) {
		// close application button
		if (x >= WINDOW_WIDTH - this.closeApplication.getWidth() && y <= (this.closeApplication.getHeight())) {
			System.exit(0);
		}
		if (applicationMode == 0) {
			TextButton startButton = startScreen.getStartButton();
			if (x >= startButton.getX() && x <= startButton.getX() + startButton.getWidth() && y >= startButton.getY()
					&& y <= startButton.getY() + startButton.getHeight()) {
				for (Player p : chooseablePlayers) {
					if (p.getAlive() == true) {
						alivePlayers++;
					}
				}
				if (alivePlayers > 1) {
					for (Player p : chooseablePlayers) {
						if (p.getAlive() == true) {
							players.add(p);
						}
					}
					applicationMode = 1;

				} else {
					alivePlayers = 0;
				}

			}
			for (TextButton t : StartScreen.buttons) {
				if (x >= t.getX() && x <= t.getX() + t.getWidth() && y >= t.getY() && y <= t.getY() + t.getHeight()) {
					chooseablePlayers.get(StartScreen.buttons.indexOf(t)).setAlive();
					t.toggleMessage();
				}
			}
		}
		if (applicationMode == 2) {
			TextButton startButton = endscreen.getStartButton();
			if (x >= startButton.getX() && x <= startButton.getX() + startButton.getWidth() && y >= startButton.getY()
					&& y <= startButton.getY() + startButton.getHeight()) {
				applicationMode = 0;
				initStartScreen();
			}
		}

	}

	public void initGlobalFonts() {
		Game.ttfHeading = new TrueTypeFont(FONT_HEADING, true);
		Game.ttfSubHeading = new TrueTypeFont(FONT_SUBHEADING, true);
		Game.ttfTableHeading = new TrueTypeFont(FONT_TABLE_HEADING, true);
		Game.ttfTableText = new TrueTypeFont(FONT_TABLE_TEXT, true);
		Game.ttfButtonText = new TrueTypeFont(FONT_BUTTON_TEXT, true);
	}

	public void initStartScreen() {
		this.startScreen = new StartScreen(0, 0, PLAYGROUND_WIDTH, WINDOW_HEIGHT, new Color(0x00006f96));
	}

	public void initPlayground() {
		chooseablePlayers = new ArrayList<>();
		players = new ArrayList<>();

		this.playground = new MyRectangle(0, 0, PLAYGROUND_WIDTH, WINDOW_HEIGHT, new Color(0x0000131a));

		chooseablePlayers.add(new Player(Input.KEY_LEFT, "<--", Input.KEY_RIGHT, "-->", Color.red, "rot", "Redhat"));
		chooseablePlayers.add(new Player(Input.KEY_A, "A", Input.KEY_D, "D", Color.green, "grün", "Greenlee"));
		chooseablePlayers
				.add(new Player(Input.KEY_H, "H", Input.KEY_K, "K", new Color(0x00fcfc02), "gelb", "Yellowstone"));

	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void delay(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void getKeyInput(GameContainer gc, int key) {
		if (gc.getInput().isKeyDown(key)) {
			System.exit(0);
		}
	}
}
