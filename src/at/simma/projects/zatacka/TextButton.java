package at.simma.projects.zatacka;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TextButton {
	private float width, height, x, y;
	private String buttonText;
	private Color backgroundColor, foregroundColor;

	public TextButton(float x, float y, float width, float height, Color backgroundColor, Color foregroundColor,
			String buttonText) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.backgroundColor = backgroundColor;
		this.foregroundColor = foregroundColor;
		this.buttonText = buttonText;
	}

	public void render(Graphics graphics) {
		/* button background */
		graphics.setColor(this.backgroundColor);
		graphics.fillRect(this.x, this.y, this.width, this.height);
		
		/* button text */
		Game.ttfButtonText.drawString(this.x + (this.width - Game.ttfButtonText.getWidth(this.buttonText)) / 2,
				this.y + (this.height - Game.ttfButtonText.getHeight(this.buttonText)) / 2, this.buttonText,
				this.foregroundColor);
	}
	
	public void toggleMessage() {
		if (this.buttonText == "ausgewählt")
			this.buttonText = "nicht ausgewählt";
		else
			this.buttonText = "ausgewählt";
	}
	
	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
}
