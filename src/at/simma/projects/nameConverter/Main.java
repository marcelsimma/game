package at.simma.projects.nameConverter;

public class Main {

	public static void main(String[] args) {
		String i = "2018-2019_AUT";
		i = encrypt(i);
		System.out.println(i);
		i = decrypt(i);
		System.out.println(i);
	}
	
	public static String encrypt(String string) {
		int stringLength = string.length();
		String stringResult = "";
		
		for (int i = 0; i < stringLength; i++) {
			stringResult += ((int)(string.charAt(i)) - stringLength%16) + "-";
		}

		return stringResult;
	}

	public static String decrypt(String string) {
		String[] parts = string.split("-");

		String str = "";
		int tmp;
		
		for (int i = 0; i < parts.length; i++) {
			tmp = Integer.valueOf(parts[i])+parts.length%16;
			str += Character.toString((char)tmp);
		}

		return str;
	}


}
